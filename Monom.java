package Domain;

public class Monom 
{
	private int coeficient;
	private int power;

	public Monom(int coeficent, int power) 
	{
		this.setCoeficient(coeficent);
		this.setPower(power);
	}
	
	public int getCoeficient() 
	{
		return this.coeficient;
	}
	
	public int getPower() {
		return this.power;
	}
	
	public void setCoeficient(int coeficient) 
	{
		this.coeficient = coeficient;
	}
	
	public void setPower(int power)
	{
		this.power = power;
	}
	
	// converteste in string monomul in vederea afisarii lui
	// trecand prin toate cazurile posibile
	public String toString(boolean skipPlus) 
	{
		int coeficient = this.getCoeficient();
		int power = this.getPower();
		String toReturn = "";
		
		if(coeficient == 0 && skipPlus == true)
			return "0";
		else if (coeficient != 0){
			if(coeficient > 0 && skipPlus == false)
				toReturn = "+";
			if(coeficient == -1 && power != 0)
				toReturn = "-";
			else if(coeficient == -1 && power == 0)
				toReturn = toReturn.concat(String.valueOf(coeficient));
			if(coeficient != 1 && coeficient != -1)
				toReturn = toReturn.concat(String.valueOf(coeficient));
			if(power > 0)
			{
				toReturn = toReturn.concat("X");
				if(this.power > 1) 
					toReturn = toReturn.concat("^").concat(Integer.toString(power));
			}
			if(coeficient==1 && power==0)
				return "+1";
			
		}

		return toReturn;
	}
	
}

