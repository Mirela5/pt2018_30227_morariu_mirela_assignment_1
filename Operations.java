package Domain;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Domain.Monom;
import Domain.Polinom;

public class Operations
{ //Toate operatiile sunt implementate in prima faza pe monoame,
// iar dupa aceea extinse la polinoame

	// Adunarea poloninoamelor se face adunand fiecare monom de grad egal al fiecarui
	// polinom in ordine descrescatoare. De la gradul maxim pana la gradul minim
	static public Polinom add(Polinom p, Polinom q) 
	{
		Polinom t = new Polinom("P + Q");
		
		int rank= Math.max(p.getMaxRank(), q.getMaxRank());
		for(int i = rank; i >= 0; i--)
		{
			t.addMonom(Operations.addMonoms(p.getMonomByRank(i),q.getMonomByRank(i)));
		}
				
		return t;
	}
	
	static public Polinom substract(Polinom p, Polinom q) 
	{
		Polinom t = new Polinom("P - Q");
		
		int rank= Math.max(p.getMaxRank(), q.getMaxRank());
		for(int i = rank; i >= 0; i--) 
		{
			t.addMonom(Operations.substractMonoms(p.getMonomByRank(i),q.getMonomByRank(i)));
		}
				
		return t;
	}
	
	static public Polinom multiplicate(Polinom p, Polinom q) 
	{
		Polinom t = new Polinom("P * Q");
		Monom m;
		for(int i = 0 ; i <= p.getMaxRank() ; i++) 
		{
			for(int j = 0 ; j <= q.getMaxRank() ; j++) 
			{
				m = Operations.multiplicateMonoms(p.getMonomByRank(i), q.getMonomByRank(j));
				if(m.getCoeficient() != 0)
					t.addMonom(m);
			
			}
		}
		return t;
	}
	
	static public Polinom derivate(Polinom p)
	{
		Polinom t = new Polinom(p.getName().concat("'"));
		Monom m;
		for(int i = 0 ; i <= p.getMaxRank() ; i++)
		{
			m = Operations.derivateMonoms(p.getMonomByRank(i));
			if(m.getCoeficient() != 0)
				t.addMonom(m);
		}
		return t;
	}
	
	static public Polinom integrate(Polinom p) 
	{
		Polinom t = new Polinom(p.getName().concat("int "));
		Monom m;
		for(int i = 0 ; i <= p.getMaxRank() ; i++)
		{
			m = Operations.integrateMonoms(p.getMonomByRank(i));
			if(m.getPower() != 0)
				t.addMonom(m);
				
				
		}
		return t;
	}
	
	
	static public Monom addMonoms(Monom p, Monom q) 
	{
		return new Monom(p.getCoeficient() + q.getCoeficient(), p.getPower());
		
	}

	static public Monom substractMonoms(Monom p, Monom q) 
	{
		return new Monom(p.getCoeficient() - q.getCoeficient(), p.getPower());
		
	}
	
	static public Monom multiplicateMonoms(Monom p, Monom q) 
	{		
		return new Monom(p.getCoeficient() * q.getCoeficient(), p.getPower() + q.getPower());
		
	}
	
	static public Monom derivateMonoms(Monom p)
	{
		if(p.getPower() == 0)
			return new Monom(0,0);
		return new Monom(p.getCoeficient() * p.getPower(), p.getPower() - 1 );
		
	}
	
	static public Monom integrateMonoms(Monom p ) 
	{
		if(p.getPower() == 0)
			return new Monom(p.getCoeficient(),1);
		return new Monom((p.getCoeficient() / (p.getPower() + 1)), p.getPower() + 1 );
		
		
	}
	
	static public int getCoeficientFromString(String s) 
	{
		int index = s.indexOf("X");
		if(index == -1) 
			return Integer.parseInt(s);
		s = s.substring(0, index);
		if(s.compareTo("") == 0 || s.compareTo("+") == 0 ) 
    		return 1;
		
		return Integer.parseInt(s);	
		
	}
	
	
	static public int getRankFromString(String s)
	{
		int index = s.indexOf("X");
		if(index == -1)
			return 0;
		if(index >= s.length() - 2)
			return 1;
		
		s = s.substring(index+2,s.length());
		return Integer.parseInt(s);
	}
	
	static public Polinom getPolinom(String inputText, String name)
	{
		Polinom thePolinom = new Polinom(name);
		Pattern p = Pattern.compile("[+-]?\\d*[xX]?[\\^]?\\d*");
		Matcher m = p.matcher(inputText);
		String s = "";
		while(m.find()) 
		{
			s = m.group(0).toUpperCase();
			if(s.compareTo("") != 0) 
			{
				thePolinom.addMonom(new Monom(Operations.getCoeficientFromString(s), Operations.getRankFromString(s)));
			}
		}
		return thePolinom;
	}
	
}

