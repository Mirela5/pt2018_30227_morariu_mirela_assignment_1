package Domain;

import java.util.ArrayList;
import java.util.List;

import Domain.Operations;

public class Polinom {
	//un polinom este definit cu un nume unic si o lista de monoame
	ArrayList<Monom> monoms;
	String name;
	
//  ---------------------------
	public Polinom(String name)
	{
		this.name = name;
		this.monoms = new ArrayList<Monom>();
	}
	
//  ---------------------------
	public void addMonom(Monom m)
	{
		Monom sameRank = this.getMonomByRank(m.getPower());
		m = Operations.addMonoms(m,sameRank);
		this.deleteMonom(sameRank);
		monoms.add(m);
	}
	
	public void deleteMonom(Monom m) 
	{
		monoms.remove(m);
	}
	
	public void editMonom(Monom m,int coeficient, int power) 
	{
		m.setCoeficient(coeficient);
		m.setPower(power);
	}
	
	public Monom getMonomByRank(int rank) 
	{
		for(Monom t: monoms) 
		{ 
			if(t.getPower() == rank) 
			{
				return t;
			}
		}
		return new Monom(0,rank);
	}
	
	public int getMaxRank() 
	{
		int maxRank = 0;
		for(Monom t: monoms) 
		{
			if(t.getPower() > maxRank) 
			{
				maxRank = t.getPower();
			}
		}
		return maxRank;
	}

	public String toString() 
	{
		String toReturn = this.name.concat(" = ");
		boolean skipPlus = true;
		for (Monom m : monoms) 
		{
			toReturn = toReturn.concat(m.toString(skipPlus));
			skipPlus = false;
		}
		return toReturn;
	}
	
	public String getName() 
	{
		return this.name;
	}
	
	
	
	
	
	
	

}
