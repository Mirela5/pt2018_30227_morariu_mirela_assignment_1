package UI;

import javax.swing.*;
import java.awt.event.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Domain.Monom;
import Domain.Polinom;
import Domain.Operations;

import java.text.NumberFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.border.*;

public class GUI extends JFrame
{

	JButton button1;
	JLabel label0, label1, label2, label3;
	JTextField textField1, textField2;
	JTextArea textArea;
	JScrollPane scrollPane;
	JRadioButton addNums, subtractNums, multNums, divideNums, integrateNums;
	JPanel thePanel, thePanel0, thePanel1, thePanel2, thePanel3;
	private final static String newline = "\n";

	public GUI() 
	{
		this.setSize(500, 400);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Polinoame");

		thePanel = new JPanel();
		thePanel0 = new JPanel();
		thePanel1 = new JPanel();
		thePanel2 = new JPanel();
		thePanel3 = new JPanel();

		Cerinta();
		Polinoame();
		Operatii();
		Output();
		Mainframe();

	}

	public void Mainframe() 
	{

		thePanel.add(thePanel0);
		thePanel.add(thePanel1);
		thePanel.add(thePanel2);
		thePanel.add(thePanel3);
		this.add(thePanel);
		this.setVisible(true);
		textField1.requestFocus();

	}

	public void Cerinta() 
	{

		label0 = new JLabel();
		label0.setText("Introduceti polinoame\n");
		thePanel0.add(label0);

	}

	public void Polinoame() 
	{
		thePanel = new JPanel();

		label1 = new JLabel("P : ");
		thePanel1.add(label1);
		textField1 = new JTextField("", 9);
		thePanel1.add(textField1);

		label2 = new JLabel("Q : ");
		thePanel1.add(label2);
		textField2 = new JTextField("", 9);
		thePanel1.add(textField2);

		button1 = new JButton("Calculeaza");
		ListenForButton lForButton = new ListenForButton();
		button1.addActionListener(lForButton);
		thePanel1.add(button1);

	}

	public void Operatii() 
	{
		addNums = new JRadioButton("Adunare");
		subtractNums = new JRadioButton("Scadere");
		multNums = new JRadioButton("Inmultire");
		divideNums = new JRadioButton("Derivata");
		integrateNums = new JRadioButton("Integrala");

		ButtonGroup operation = new ButtonGroup();
		operation.add(addNums);
		operation.add(subtractNums);
		operation.add(multNums);
		operation.add(integrateNums);
		operation.add(divideNums);

		JPanel operPanel = new JPanel();
		Border operBorder = BorderFactory.createTitledBorder("Operatie");
		operPanel.setBorder(operBorder);
		operPanel.add(addNums);
		operPanel.add(subtractNums);
		operPanel.add(multNums);
		operPanel.add(divideNums);
		operPanel.add(integrateNums);

		addNums.setSelected(true);
		thePanel2.add(operPanel);
	}

	public void Output()
	{
		textArea = new JTextArea(7, 35);
		JScrollPane scrollPane = new JScrollPane(textArea);
	    scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		textArea.setEditable(false);
		//thePanel3.add(textArea);
		thePanel3.add(scrollPane);
	}

	public static void main(String[] args) 
	{
		new GUI();
	}

	private class ListenForButton implements ActionListener
	{
		
		
		public void actionPerformed(ActionEvent e) 
		{

			if (e.getSource() == button1) {
				Polinom p1 = new Polinom("P"),p2 = new Polinom("Q");
				try 
				{

					p1 = Operations.getPolinom(textField1.getText(),"P");
					p2 = Operations.getPolinom(textField2.getText(),"Q");
					
				}

				catch (NumberFormatException excep) 
				{

					textArea.append("Please Enter the Right Info\n");

				}
				
				if (addNums.isSelected()) 
				{
					textArea.append(p1.toString() + newline);
					textArea.append(p2.toString() + newline);
					Polinom result = Operations.add(p1, p2);
					textArea.append(result.toString() + newline);

				} else if (subtractNums.isSelected())
				{
					textArea.append(p1.toString() + newline);
					textArea.append(p2.toString() + newline);
					Polinom result = Operations.substract(p1, p2);
					textArea.append(result.toString() + newline);

				} else if (multNums.isSelected())
				{
					textArea.append(p1.toString() + newline);
					textArea.append(p2.toString() + newline);
					Polinom result = Operations.multiplicate(p1, p2);
					textArea.append(result.toString() + newline);

				} else if(divideNums.isSelected())
				
				{
					textArea.append(p1.toString() + newline);
					Polinom result = Operations.derivate(p1);
					textArea.append(result.toString() + newline);
				} 
				else if(integrateNums.isSelected())
				{
					textArea.append(p1.toString() + newline);
					Polinom result = Operations.integrate(p1);
					textArea.append(result.toString() + newline);
				}

				// JOptionPane.showMessageDialog(GUI.this, totalCalc, "Solution",
				// JOptionPane.INFORMATION_MESSAGE);

			}

		}

	}

	

}
